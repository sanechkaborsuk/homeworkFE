const express = require('express');
const router = new express.Router();

const errorHandling = require('./middleWares/errorHandling');
const {authMiddleware} = require('./middleWares/authMiddleware');
const {
  getAllNotes,
  addNewNote,
  getNoteById,
  updateNoteById,
  checkNoteById,
  deleteNoteById,
} = require('../controllers/noteController');

router.use(authMiddleware);

router.get('/', getAllNotes);
router.post('/', addNewNote);
router.get('/:id', getNoteById);
router.put('/:id', updateNoteById);
router.patch('/:id', checkNoteById);
router.delete('/:id', deleteNoteById);

router.use(errorHandling);
module.exports = router;
