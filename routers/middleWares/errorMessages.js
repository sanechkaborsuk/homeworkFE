module.exports = {
  authProblem: 'No Authorization http header found!',
  tokenProblem: 'No JWT token found!',
  invalidProblem: 'Invalid input due to rules.' +
  ' Min length for username - 3.' +
  ' Min length for password - 6',
  requiredFields: 'Missing required fields',
  noUser: 'No user with such username',
  wrongPassword: 'Wrong password!',
  noNote: 'Can not find any note',
};
