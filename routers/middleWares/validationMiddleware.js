const Joi = require('joi');

const {BadRequest} = require('./errors');
const {invalidProblem} = require('./errorMessages');

module.exports.validateRegistration = async (req, res, next) => {
  try {
    const schema = Joi.object({
      username: Joi.string()
          .alphanum()
          .required()
          .error(new BadRequest(invalidProblem)),

      password: Joi.string()
          .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
          .error(new BadRequest(invalidProblem)),
    });
    await schema.validateAsync(req.body);
    next();
  } catch (error) {
    next(error);
  }
};
