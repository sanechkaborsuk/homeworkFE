const express = require('express');
const router = new express.Router();

const errorHandling = require('./middleWares/errorHandling');
const {validateRegistration} = require('./middleWares/validationMiddleware');
const {login, registration} = require('../controllers/authController');

router.post('/register', validateRegistration, registration);
router.post('/login', login);

router.use(errorHandling);

module.exports = router;
