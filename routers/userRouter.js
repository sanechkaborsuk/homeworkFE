const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('./middleWares/authMiddleware');
const errorHandling = require('../routers/middleWares/errorHandling');

const {
  getProfile,
  deleteProfile,
  changePassword,
} = require('../controllers/userController');

router.use(authMiddleware);

router.get('/', getProfile);
router.delete('/', deleteProfile);
router.patch('/', changePassword);

router.use(errorHandling);

module.exports = router;
