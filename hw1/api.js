const fs = require('fs').promises;
const path = require('path');
const express = require('express');

const app = express.Router();

const handleErrors = require('./handleErrors');
const { BadRequest } = require('./errors');
const { requiredFields, wrongExtension } = require('./errorMessage');

const availableExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

// *Create file
app.post('/', async(request, response, next) => {
    try {
        const {filename, content} = request.body;

        if (!filename || !content) throw new BadRequest(requiredFields);

        const fileExtName = filename.split('.').pop();
        if(!availableExtensions.includes(fileExtName)){
            throw new BadRequest(wrongExtension);
        }

        await fs.writeFile(path.join("files", filename), content, 'utf-8');
        response.status(200).json({ "message": "File created successfully" });

    } catch (error) {
        next(error);
    }
});



//* Get list of files
app.get('/', async(request, response, next) => {
    try {
        const array = await fs.readdir("files");
        if(array.length === 0) throw new BadRequest('There are no files in folder');

        response.status(200).json({ "message": "Success", "files": array });
    } catch (error) {
        next(error);
    }    
});



//* Get detailed info by filename
app.get('/:filename', async(request, response, next) => {
    try {
        const filename = request.params.filename;

        const fileExtName = filename.split('.').pop();
        if(!availableExtensions.includes(fileExtName)){
            throw new BadRequest(wrongExtension);
        }

        const filedata = await fs.readFile(path.join("files", filename), 'utf-8', (error) => {
            if (error) throw new BadRequest (`No file with ${filename} filename found`);
        });

        const { birthtime } = await fs.stat(path.join("files", filename));

        response.status(200).json({ 
            "message": "Success",
            "filename": filename,
            "content": filedata,
            "extension": fileExtName,
            "uploadedDate": birthtime
        });

    } catch (error) {
        next(error);
    }
});



//* Delete file by filename (optional task)
app.delete('/:filename', async(request, response, next) => {
    try {
        const filename = request.params.filename;

        const fileExtName = filename.split('.').pop();
        if(!availableExtensions.includes(fileExtName)){
            throw new BadRequest(wrongExtension);
        }

        await fs.unlink(path.join(__dirname, "files", filename), error => {
            if (error) throw new BadRequest (`Can not delete file ${filename}`);    
        });

        response.status(200).json({ "message": `Delete file ${filename} successfully` });
        
    } catch (error) {
        next(error);
    }
});



//* Modify file by filename (optional task)
app.patch('/modify', async(request, response, next) => {
    try {
        const {filename, addContent} = request.body;

        if (!filename || !addContent) throw new BadRequest(requiredFields);

        const fileExtName = filename.split('.').pop();
        if(!availableExtensions.includes(fileExtName)){
            throw new BadRequest(wrongExtension);
        }

        await fs.appendFile(path.join(__dirname, "files", filename), addContent, 'utf-8', (error) => {
            if(error) throw new BadRequest (`Something went wrong, check your input`);
        });
        
        response.status(200).json({ "message": `Modify file ${filename} successfully` });
    } catch (error) {
        next(error);
    }
});

app.use(handleErrors);

module.exports = app;