const express = require('express');
const logger = require('morgan');
const app = express();

const api = require('./api');

app.use(express.json());
app.use(logger('combined'));
app.use('/api/files', api);

app.listen(8080, '127.0.0.1');