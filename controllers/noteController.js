const {Note} = require('../models/noteModel');

const {BadRequest} = require('../routers/middleWares/errors');
const {
  requiredFields,
  noNote,
} = require('../routers/middleWares/errorMessages');

module.exports.getAllNotes = async (request, response, next) => {
  try {
    const {offset = 0, limit = 5} = request.query;
    const userNotes = await Note.find(
        {userId: request.user._id},
        [],
        {
          skip: Number(offset),
          limit: Number(limit),
        },
    );
    response.status(200).json({notes: userNotes});
  } catch (error) {
    next(error);
  }
};

module.exports.addNewNote = async (request, response, next) => {
  try {
    const {text} = request.body;
    if (!text) throw new BadRequest(requiredFields);

    const newNote = new Note({
      userId: request.user._id,
      text: text,
    });
    await newNote.save();
    response.status(200).json({message: 'Note is created successfully!'});
  } catch (error) {
    next(error);
  }
};

module.exports.getNoteById = async (request, response, next) => {
  try {
    const id = request.params.id;
    const userNote = await Note.findById(id);
    if (!userNote) throw new BadRequest(noNote);

    response.status(200).json({note: userNote});
  } catch (error) {
    next(error);
  }
};

module.exports.updateNoteById = async (request, response, next) => {
  try {
    const id = request.params.id;
    const {text} = request.body;
    if (!text) throw new BadRequest(requiredFields);

    await Note.findByIdAndUpdate(id, {$set: {text: text}});
    response.status(200).json({message: 'Note is updated successfully!'});
  } catch (error) {
    next(error);
  }
};

module.exports.checkNoteById = async (request, response, next) => {
  try {
    const id = request.params.id;
    const {completed} = await Note.findById(id);

    await Note.findByIdAndUpdate(id, {$set: {completed: !completed}});
    response.status(200).json({
      message: 'Note status is updated successfully!',
    });
  } catch (error) {
    next(error);
  }
};

module.exports.deleteNoteById = async (request, response, next) => {
  try {
    const id = request.params.id;
    await Note.findByIdAndDelete(id);
    response.status(200).json({message: 'Note is deleted successfully!'});
  } catch (error) {
    next(error);
  }
};
