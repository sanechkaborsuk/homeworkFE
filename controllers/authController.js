const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

const {BadRequest} = require('../routers/middleWares/errors');
const {
  requiredFields,
  noUser,
  wrongPassword,
} = require('../routers/middleWares/errorMessages');

module.exports.registration = async (request, response, next) => {
  try {
    const {username, password} = request.body;
    const user = new User({
      username,
      password: await bcrypt.hash(password, 5),
    });

    await user.save();

    response.status(200).json({message: 'User is created successfully!'});
  } catch (error) {
    next(error);
  }
};

module.exports.login = async (request, response, next) => {
  try {
    const {username, password} = request.body;
    if (!username || !password) throw new BadRequest(requiredFields);

    const user = await User.findOne({username});
    if (!user) throw new BadRequest(noUser);

    if ( !(await bcrypt.compare(password, user.password)) ) {
      throw new BadRequest(wrongPassword);
    }

    const token = jwt.sign({
      _id: user._id,
      username: user.username,
      createdDate: user.createdDate,
    }, JWT_SECRET);

    response.status(200).json({message: 'Success', jwt_token: token});
  } catch (error) {
    next(error);
  }
};
