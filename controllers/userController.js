const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const {BadRequest} = require('../routers/middleWares/errors');
const {wrongPassword} = require('../routers/middleWares/errorMessages');

module.exports.getProfile = (request, response, next) => {
  try {
    response.status(200).json({user: request.user});
  } catch (error) {
    next(error);
  }
};

module.exports.deleteProfile = async (request, response, next) => {
  try {
    const deleteID = request.user._id;
    await User.findByIdAndDelete(deleteID);
    response.status(200).json({message: 'Delete user successfully!'});
  } catch (error) {
    next(error);
  }
};

module.exports.changePassword = async (request, response, next) => {
  try {
    const {oldPassword, newPassword} = request.body;
    const user = await User.findOne({_id: request.user._id});

    if ( !(await bcrypt.compare(oldPassword, user.password)) ) {
      throw new BadRequest(wrongPassword);
    }

    await User.findByIdAndUpdate(user._id,
        {$set: {password: await bcrypt.hash(newPassword, 5)}});

    response.status(200).json({message: 'Change password successfully!'});
  } catch (error) {
    next(error);
  }
};
