const express = require('express');
const logger = require('morgan');
const mongoose = require('mongoose');
const app = express();
const PORT = process.env.PORT || 8080;

require('dotenv').config();

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const noteRouter = require('./routers/noteRouter');

app.use(express.json());
app.use(logger('combined'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/notes', noteRouter);

const start = async () => {
  await mongoose.connect('mongodb+srv://test_user:test@cluster0.s0nrs.mongodb.net/hw2?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  // console.log(process.env);
  app.listen(PORT, () => {
    console.log(`Server works at port ${PORT}!`);
  });
};

start();
