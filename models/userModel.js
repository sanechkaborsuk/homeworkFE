const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    uniq: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    default: Date.now(),
  },
});

module.exports.User = mongoose.model('User', userSchema);
